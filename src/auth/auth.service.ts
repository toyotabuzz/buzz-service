import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { UsersAuthEntity } from 'src/entities/buzzservice/userAuth.entity';
import { LoginDTO } from 'src/dto/buzz_service/login.dto';
import { JwtService } from '@nestjs/jwt';
import { RegisterAuthDTO } from 'src/dto/buzz_service/registerAuth.dto';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UsersAuthEntity)
    private usersRepository: Repository<UsersAuthEntity>,
    private jwtService: JwtService,
  ) {}

  async register(params: RegisterAuthDTO) {
    try {
      // check duplicate username
      const userDuplicate = await this.usersRepository.findOne({
        where: {
          username: params.username,
          isactive: 'Y',
        },
      });

      if (userDuplicate) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          success: false,
          response: '!Username is duplicate',
        };
      }

      let register = await this.usersRepository.create(params);
      let result = await this.usersRepository.save(register);
      if (!result) {
        return {
          statusCode: HttpStatus.BAD_REQUEST,
          success: false,
          response: '!Register fail',
        };
      } else {
        return {
          statusCode: HttpStatus.OK,
          success: true,
          response: 'Register successfully.',
          data: { id: result.id },
        };
      }
    } catch (e) {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        success: false,
        response: e.message,
      };
    }
  }

  async login(param: LoginDTO) {
    try {
      const user = await this.usersRepository.findOne({
        where: {
          username: param.username,
          isactive: 'Y',
        },
      });
      if (!user) {
        return {
          statusCode: HttpStatus.UNAUTHORIZED,
          success: false,
          response: '!Username do not exit!',
        };
      }

      const isMatch = await bcrypt.compare(param.password, user.password);
      if (!isMatch) {
        return {
          statusCode: HttpStatus.UNAUTHORIZED,
          success: false,
          response: 'Password is incorrect!',
        };
      }

      let res = this.signInJwt(user.id, user.username); // Get JWT Token

      return {
        statusCode: HttpStatus.OK,
        success: true,
        response: 'Login successfully.',
        tokenId: res,
        data: { id: user.id },
      };
    } catch (e) {
      return {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        success: false,
        response: e.message,
      };
    }
  }

  signInJwt(id: number, username: string) {
    return this.jwtService.sign({
      sub: id,
      username: username,
    });
  }
}
