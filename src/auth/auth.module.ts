import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppService } from 'src/app.service';
import { UsersAuthEntity } from 'src/entities/buzzservice/userAuth.entity';
import { ApiLogEntity } from 'src/entities/buzzservice/apiLog.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt.strategy';
require('dotenv').config();

@Module({
  imports: [
    TypeOrmModule.forFeature([UsersAuthEntity, ApiLogEntity]),
    PassportModule,
    JwtModule.register({
      secret: process.env.SECRET_KEY,
      signOptions: { expiresIn: '1m' },
    }),
  ],
  controllers: [AuthController],
  providers: [AppService, AuthService, JwtStrategy],
})
export class AuthModule {}
