import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';

import { LoginDTO } from 'src/dto/buzz_service/login.dto';
import { RegisterAuthDTO } from 'src/dto/buzz_service/registerAuth.dto';
import { AppService } from 'src/app.service';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private appService: AppService,
  ) {}

  @Post('register')
  async register(@Body() register: RegisterAuthDTO) {
    const res = await this.authService.register(register);

    // save log api
    const apiLogResult = await this.appService.saveApiLog({
      method_name: 'auth/register',
      token_id: null,
      payload: JSON.stringify(register),
      result: JSON.stringify(res),
      created_by: !res.success ? null : res.data.id,
      updated_by: !res.success ? null : res.data.id,
    });

    return res;
  }

  @Post('login')
  async login(@Body() login: LoginDTO) {
    const res = await this.authService.login(login);

    // save log api
    const apiLogResult = await this.appService.saveApiLog({
      method_name: 'auth/login',
      token_id: null,
      payload: JSON.stringify(login),
      result: JSON.stringify(res),
      created_by: !res.success ? null : res.data.id,
      updated_by: !res.success ? null : res.data.id,
    });

    return res;
  }
}
