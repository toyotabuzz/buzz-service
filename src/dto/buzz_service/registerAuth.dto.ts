export interface RegisterAuthDTO {
  username: string;
  password: string;
  firstname: string;
  lastname: string;
  nickname: string;
}
