export interface ApiLogDTO {
  method_name: string;
  token_id: string;
  payload: string;
  result: string;
  created_by: number;
  updated_by: number;
}
