import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Index,
  BeforeInsert,
} from 'typeorm';

import * as bcrypt from 'bcrypt';
import * as helpers from 'src/Helpers/helpers';

@Entity({ database: 'buzz_service', name: 'tuuser_auth' })
export class UsersAuthEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Index('tuuser_auth_username_idx')
  @Column({
    type: 'varchar',
    length: 20,
  })
  username: string;

  @Column({
    type: 'varchar',
    length: 50,
  })
  password: string;

  @Column({
    type: 'varchar',
    length: 100,
  })
  firstname: string;

  @Column({
    type: 'varchar',
    length: 100,
  })
  lastname: string;

  @Column({
    type: 'varchar',
    length: 50,
  })
  nickname: string;

  @Column({
    type: 'char',
    length: 1,
    default: 'Y',
  })
  isactive: string;

  @Column({
    type: 'datetime',
  })
  created_at: string;

  @Column({
    type: 'int',
  })
  created_by: number;

  @Column({
    type: 'datetime',
  })
  updated_at: string;

  @Column({
    type: 'int',
  })
  updated_by: number;

  @BeforeInsert()
  async defalutData() {
    let salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);
    this.isactive = helpers.isActive();
    this.created_at = helpers.dateNow();
  }
}
