import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  Index,
  BeforeInsert,
} from 'typeorm';

import * as helpers from 'src/Helpers/helpers';

@Entity({ database: 'buzz_service', name: 'juapi_log' })
export class ApiLogEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Index('juapi_log_method_name_idx')
  @Column({
    type: 'varchar',
    length: 150,
  })
  method_name: string;

  @Column({
    type: 'varchar',
    length: 150,
  })
  token_id: string;

  @Column({
    type: 'text',
  })
  payload: string;

  @Column({
    type: 'text',
  })
  result: string;

  @Column({
    type: 'datetime',
  })
  created_at: string;

  @Column({
    type: 'int',
  })
  created_by: number;

  @Column({
    type: 'datetime',
  })
  updated_at: string;

  @Column({
    type: 'int',
  })
  updated_by: number;

  @BeforeInsert()
  async defalutData() {
    this.created_at = helpers.dateNow();
    this.updated_at = helpers.dateNow();
  }
}
