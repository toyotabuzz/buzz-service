import { Entity, Column, PrimaryGeneratedColumn, Index } from 'typeorm';

@Entity({ database: 'buzz_power', name: 'tuuser' })
export class UsersEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Index('idx_username')
  @Column({
    type: 'varchar',
    length: 20,
  })
  username: string;

  @Column({
    type: 'char',
    length: 32,
  })
  password: string;

  @Column({
    type: 'varchar',
    length: 10,
  })
  emp_no: string;

  @Column({
    type: 'varchar',
    length: 100,
  })
  firstname: string;

  @Column({
    type: 'varchar',
    length: 100,
  })
  lastname: string;

  @Column({
    type: 'varchar',
    length: 50,
  })
  nickname: string;

  @Column({
    type: 'varchar',
    length: 30,
  })
  firstname_en: string;

  @Column({
    type: 'varchar',
    length: 30,
  })
  lastname_en: string;

  @Column({
    type: 'varchar',
    length: 30,
  })
  nickname_en: string;

  @Column({
    type: 'date',
  })
  birth_date: string;

  @Column({
    type: 'int',
  })
  level_id: number;

  @Column({
    type: 'int',
  })
  position_id: number;

  @Column({
    type: 'int',
  })
  department_id: number;

  @Column({
    type: 'int',
  })
  branch_id: number;

  @Column({
    type: 'varchar',
    length: 100,
  })
  email: string;

  @Column({
    type: 'varchar',
    length: 20,
  })
  tel_no: string;

  @Column({
    type: 'varchar',
    length: 50,
  })
  mobile_no: string;

  @Column({
    type: 'char',
    length: 1,
    default: 'Y',
  })
  status: string;

  @Column({
    type: 'date',
  })
  expire_date: string;

  @Column({
    type: 'varchar',
    length: 50,
  })
  auth_key: string;

  @Column({
    type: 'varchar',
    length: 255,
  })
  password_hash: string;

  @Column({
    type: 'varchar',
    length: 50,
  })
  password_reset_token: string;

  @Column({
    type: 'int',
  })
  call_extension_id: number;

  @Column({
    type: 'datetime',
  })
  create_date: string;

  @Column({
    type: 'int',
  })
  create_by: number;

  @Column({
    type: 'datetime',
  })
  update_date: string;

  @Column({
    type: 'int',
  })
  update_by: number;
}
