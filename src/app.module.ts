import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { Connection } from 'typeorm';
import { UsersAuthEntity } from './entities/buzzservice/userAuth.entity';
import { ApiLogEntity } from './entities/buzzservice/apiLog.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({ name: 'default' }),
    TypeOrmModule.forRoot({ name: 'buzz_power' }),
    TypeOrmModule.forRoot({ name: 'buzz_sure' }),
    TypeOrmModule.forFeature([UsersAuthEntity, ApiLogEntity]),
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
