import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { JwtAuthGuard } from './auth/guard/jwt-guard.guard';
import { GetCurrentUserById } from './utilities/get-user-by-id.decorator';
import { Request } from 'express';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getIndex() {
    return 'Hello guy';
  }

  @UseGuards(JwtAuthGuard)
  @Get('getuser')
  async getUser(@GetCurrentUserById() userId: number, @Req() request: Request) {
    const result = await this.appService.userById(userId);

    // save log api
    const toketClient = request.headers.authorization.replace('Bearer ', '');
    const apiLogResult = await this.appService.saveApiLog({
      method_name: 'getuser',
      token_id: toketClient,
      payload: JSON.stringify(userId),
      result: JSON.stringify(result),
      created_by: userId,
      updated_by: userId,
    });
    return result;
  }
}
