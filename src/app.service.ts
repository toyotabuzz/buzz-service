import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ApiLogDTO } from './dto/buzz_service/apiLog.dto';
import { UsersAuthEntity } from './entities/buzzservice/userAuth.entity';
import { ApiLogEntity } from './entities/buzzservice/apiLog.entity';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(UsersAuthEntity)
    private usersRepository: Repository<UsersAuthEntity>,

    @InjectRepository(ApiLogEntity)
    private apilogRepository: Repository<ApiLogEntity>,
  ) {}

  async userById(userId: number) {
    const user = await this.usersRepository.findOne({ where: { id: userId } });
    const { id, username, firstname, lastname, nickname } = user;
    const result = {
      id,
      username,
      firstname,
      lastname,
      nickname,
    };
    return result;
  }

  async saveApiLog(apiLog: ApiLogDTO) {
    const createLog = await this.apilogRepository.create(apiLog);
    const resultLog = await this.apilogRepository.save(createLog);
    console.log(resultLog);
  }
}
