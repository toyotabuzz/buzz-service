import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.setGlobalPrefix(process.env.PREFIX_APP);
  await app.listen(process.env.PORT);
}
bootstrap();
